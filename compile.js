const path = require('path');
const fs = require('fs');
const solc = require('solc');

const contractPath = path.resolve(__dirname, '', 'MyNewContractWithTransaction.sol');
const source = fs.readFileSync(contractPath, 'utf8');
const compiled = solc.compile(source, 1);
console.log(compiled);
module.exports = compiled.contracts[':MyNewContractWithTransaction'];
