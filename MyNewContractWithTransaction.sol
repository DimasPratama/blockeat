pragma solidity >=0.4.22 <0.6.0;

contract MyNewContractWithTransaction {
    address public contractOwner;
    address public lastDonatorAddress;
    uint public lastDonatedAmount;

    constructor() public {
        contractOwner = msg.sender;
    }

    function sayMyAddress() public view returns (address) {
        return msg.sender;
    }

    function donate() public payable {
        lastDonatorAddress = msg.sender;
        lastDonatedAmount = msg.value;
    }

    // membuat function getDonationBalance yang me-return balance dari contract
    function getDonationBalance() public view returns (uint) {
        // untuk menggunakan .balance, kita membutuhkan sebuah address
        // <address>.balance, dapat digunakan baik untuk account ataupun contract
        // karena kita mau me-return balance dari contract ini, bukan dari account tertentu,
        // maka kita bisa gunakan address(this) untuk mendapakan address dari contract ini.
        return address(this).balance;
    }
    //membuatfunction untuk mengetahui sisa balance donator
    function getDonatorBalance()public view returns(uint){
        return address(msg.sender).balance;
    }
}
